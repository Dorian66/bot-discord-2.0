//------------------------------------------------------------- initialisation et sécurité du bot ---------------------------------------------------------------------------------------


// ajout de la librairie discord
const Discord = require('discord.js'),
     client = new Discord.Client({
        fetchAllMembers: true,
     }),
     config = require('./config.json'),// connection du bot avec le token 
     fs = require('fs'); 

client.login(config.token);// connection du bot 
client.commands = new Discord.Collection();

fs.readdir('./commands', (err, files ) => {
   if (err) throw err;
   files.forEach(file => {
      if (!file.endsWith('.js')) return;
      const command = require (`./commands/${file}`);
      client.commands.set(command.name, command);
      
   });
});



//--------------------------------------------- commande du bot -----------------------------------------------------------------------------
// a l'envoie d'un message 
client.on('message', message => {
   if(message.type !== 'DEFAULT' || message.author.bot) return; // si c'est un message par défault ou un message du bot 

   const args = message.content.trim().split(/ + /g); // séparé l'argument de la commande [!test], [15612] 

   const commandName = args.shift().toLowerCase(); // au cas ou si ca capslock  
   if ( !commandName.startsWith(config.prefix) ) return;// si ya pas de prefixe ya rien 
   const command = client.commands.get(commandName.slice(config.prefix.length)); // ca sépare le prefix avec l'arguments 
   if (!command) return; // si on a pas trouvé de commande c'est une commande qui n'existe pas 
   command.run(message, args, client);
   
 
});
//------------------------------------------------------------- Message de bienvenue et de Dépaclient ---------------------------------------------------------------------------------------------------------------------

// Arrivé des personnes 
client.on('guildMemberAdd', member => {
   member.guild.channels.cache.get(config.greeting.channel).send(`Bienvenue ${member} Jeune flambeur , Ce larbin de PriMath oscille comme Charles Baudelaire !  allez dans #regle coché la case validé trouver le code et entré le code dans #verification, Nous sommes  ${member.guild.memberCount} flambeur !`);
   member.roles.add(config.greeting.role);
});

// Départ des personnes
client.on('guildMemberRemove', member => {
   member.guild.channels.cache.get(config.greeting.channel).send(`${member.user.tag} prend sa blagues avec lui Que dit un rappeur quand il rentre dans une fromagerie ? Faites du brie ! `);

});
//--------------- envoie un message dans la console quand le bot est prêt ------------------------------------------------------------------------------------------------------
  client.on('ready', () => {
      console.log('client start');
    });




