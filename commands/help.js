const Discord = require('discord.js')

module.exports = {
    run: message => {
        message.channel.send(new Discord.MessageEmbed()
        .setTitle('Liste des commandes')
        .setDescription(`
        *start: sert a savoir si le bot est fonctionnel
        *lgvg: liste toutes les personnes inscrite en gvg
        *fgvg: notifie le role gvg et leur demande de faire leur gvg 
        *flaby: notifie everyone et leur demande de faire leur laby 
        *help: liste toutes les commandes disponibles sur le bot
        *ngvg: Liste des joueurs qui n'ont pas fait leur gvg
        *i: Pour vous inscrire en GVG et GVO
        `)
        )
    },
    name: 'help'
}