const { Client, Message, MessageEmbed } = require("discord.js");

module.exports = {
    run: async (client, message, args) => {
        const questions = [
            "A quel contenu de guilde veut tu t'inscrire GVG ou GVO",
            "Quel est ton pseudo en jeux ?",
            "Combien de monstre as tu en 6 etoile ?",
            "Combien de monstre as tu runé pour le contenu de guilde ?",
            "Quel defence utilise-tu ?",
            "Quel sont tes horraires de dispo ?",
            ];

            let collectCounter = 0;
            let endCounter = 0;

            const filter = (m) => m.author.id === client.author.id;
            
            const appStart = await client.author.send(questions[collectCounter++]);
            const  channel = appStart.channel;
            
            const collector = channel.createMessageCollector(filter);
            // verifie si le questionnaire a été rempli ou non 
            collector.on("collect", () => {

                  if(collectCounter < questions.length) {
                    channel.send(questions[collectCounter++]);
                } else {
                    channel.send(`L'inscription a été envoyé`);
                     collector.stop("fulfilled");
                    
            }
            });
            
              
                
            const appsChannel =  client.guild.channels.cache.get('830821580564987905');
            collector.on('end', (collected, reason) => {
                if(reason === 'fulfilled') {
                    let index = 1;
                    const mappedResponses = collected.map((message) => {
                        return `${index++}) ${questions[endCounter++]}\n-> ${message.content}`;
                    })
                    .join("\n\n");

                    appsChannel.send(
                        new MessageEmbed()
                        .setAuthor(client.author.tag, client.author.displayAvatarURL({ dynamic: true}))
                        .setTitle("Nouvelle Inscription")
                        .setDescription(mappedResponses)
                        .setColor('RANDOM')
                        .setTimestamp()

                    ) 
                }
            });
        },
        name: "i"
    };